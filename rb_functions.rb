# Functions in Ruby

def addNums(num_1, num_2)
    return num_1.to_i + num_2.to_i
end

puts addNums(3,4)

# Variables are passed by value, so their value can't be changed
# inside of a function

x = 1

def change_x(x)
    x = 4
end

change_x(x)

puts "x = #{x}"

# As a result you'll see running that x did not in fact change





# Now we'll look at some string functions
puts "Added together: #{4 + 5} \n\n"
puts 'Added together: #{4 + 5} \n\n'

# Note that the single quotes halt interpolation within the string provided

# You must do something specific with multiline strings to get them to
# actually work as intended within a variable whlie also maintaining
# interpolation, as follows:
multiline_string = <<EOM
This is a very long string
that contains interpolation
such as #{4 + 5} \n\n
EOM

puts multiline_string



first_name = "Jonah"
last_name = "Karau"

# Concatenating is a great way to combine strings.
full_name = first_name + last_name

middle_name = "James"

# As is interpolation
full_name = "#{first_name} #{middle_name} #{last_name}"

# We can also check to see if a string contains a string
puts full_name.include?("James")

# or do lots of other things with strings, since they're OBJECTS :D
puts full_name.size
puts "Vowels: " + full_name.count("aeiou").to_s         # has everything specified
puts "Consonants: " + full_name.count("^aeiou").to_s    # has everything BUT what's specified
puts full_name.start_with?("Jonah")                     # check if the string starts with something
puts "Index: " full_name.index("James").to_s            # get the index of something in the string
puts "a == a" + ("a" == "a ").to_s                      # check the equality of strings
puts "\"a\".equal?(\"a\") : " + ("a".equal?("a").to_s   # check if two objects are identical
puts first_name.equal?(first_name)
puts first_name.upcase                                  # change the casing of the string
puts first_name.downcase
puts first_name.swapcase

full_name = "            " + full_name + "          "

full_name = full_name.lstrip                            # eliminate all white space from a string, or from the left or right
full_name = full_name.rstrip
full_name = full_name.strip

puts full_name.rjust(20, ".")                           # format the text justified left, right, or centered, filling specified
puts full_name.ljust(20, ".")                           # white space with dots in this case
puts full_name.center(20, ".")

puts full_name.chop                                     # chop off the last character
puts full_name.chomp("au")                              # chomps off the specified two characters

puts full_name.delete("a")                              # delete every occurance of the letter specified