# File objects and serialization in Ruby

# Create a new file called "yourSum", and create (write) it if it doesn't already exist:
Write_Handler = File.new("yourSum.out", "w")

# Put some text into that file we just created:
Write_Handler.puts("This is some text that we just put into this file!").to_s

# Close the file after we're done working with it:
Write_Handler.close

# Retrieve the data from the file:
File_Data = File.read("yourSum.out")

# Write the data from the file to the CLI:
puts "Data from file: " + File_Data

# Load another file
load "rb_hello.rb"