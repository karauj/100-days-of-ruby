# Arithmetic operators in Ruby

    # Addition
    puts "6 + 4 = " + (6 + 4).to_s

    # Subtraction
    puts "6 - 4 = " + (6 - 4).to_s

    # Multiplication
    puts "6 * 4 = " + (6 * 4).to_s

    # Division
    puts "6 / 4 = " + (6 / 4).to_s

    # Modulus (remainder of a division)
    puts "6 % 4 = " + (6 % 4).to_s