# Looping in Ruby

x = 1

loop do
    x += 1

    # Prints out only the even numbers
    next unless (x % 2) == 0
    puts x

    break if (x >= 10)
end

y = 1

while (y <= 10)
    y += 1

    next unless (y % 2) == 0
    puts y
end

z = 1

until (z >= 10)
    z += 1

    next unless (z % 2) == 0
    puts z
end

# Using an array
numbers = [1,2,3,4,5]

for number in numbers
    puts "#{number},  "
end

numbers.each do |number|
    puts "#{number}, "
end

# Using a range
(0..5).each do |i|
    puts "#{i}, "
end