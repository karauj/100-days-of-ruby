# Integers and floats in Ruby

    # Ruby automatically converts integers to a large integer class when 
    # they overflow, so there's (practically) no limit to how big they can be.
    num_float1 = 4611686018427387903

    # Now we'll look at the accuracy of floats:
    num_floatA = 1.000

    # Floats must start with a leading decimal, even a zero.
    num_floatB = 0.999

    puts num_floatA.to_s + " - " + num_floatB.to_s + " = " + (num_floatA - num_floatB).to_s

    # Usually with floats, you can trust that the basic accuracy up to 14 places is the norm.
    num_bigFloat = 1.12345678901234

    puts (num_bigFloat + 0.00000000000005).to_s

    # As with other programming langauges, float arithmetic can be a little off, so that is
    # something to keep in mind working with extensive decimal places.