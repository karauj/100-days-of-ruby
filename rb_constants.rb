# Constants in Ruby

# Constants must start with a capital letter
A_CONSTANT = 3.14

A_CONSTANT = 1.6

puts A_CONSTANT

# We can technically change the value of the constant, even though it's not
# intended to be changed. On runtime as you will see, an error shows up, even
# though the value does in fact change. There's no safety net here, pay
# attention to how you code!