# Conditionals in Ruby

# Comparison Operators:  ==  !=  <  >  <=  >=

age = 12

if (age >= 5) && (age <= 6)
    puts "You are in kindergarten."
elsif (age >= 7) && (age <= 13)
    puts "You are in middle school."
elsif (age >= 14) && (age <= 18)
    puts "You are in high school."
else
    puts "Stay home."
end

# Unless is another comparison.
unless (age > 4)
    puts "No school."
else
    puts "You may go to school."
end

# You can do comparisons inline in outputs.
puts "You're young." if (age < 30)

# Case statements are another way to compare.
print "Enter greeting: "

# Gets the input of the user and chomps off the new line from hitting enter
greeting = gets.chomp

case greeting
when "French", "french"
    puts "Bonjour!"
    exit
when "Spanish", "spanish"
    puts "Hola!"
    exit
else # "English", "english"
    puts "Hello!"
end

# Another option to compare is the ternary operator.
puts (age >= 50) ? "Old" : "Young"

# Return 0 if both values are equal, 1 if the first value is greater than the second,
# and -1 if the first value is less than the second
puts "5 <=> 10 = " + (5 <=> 10).to_s

# Logical Operators:     && and    || or     ! not

puts "true && false = " + (true && false).to_s
puts "true || false = " + (true || false).to_s
puts "!false = " + (!false).to_s