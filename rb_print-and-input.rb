# Print and Input functionalities in Ruby

# Print something to the screen (without a new line after)
print "Enter a Value: "

    # Note: variables must start with a lowercase letter or an underscore
    # 1st int | get input | convert to int
    first_int = gets.to_i

print "Enter Another Value: "

    second_int = gets.to_i

# Print something to the screen (with a new line after)
#   convert to string | concatenate with +
puts first_int.to_s + " + " + second_int.to_s + " = " +
# arithmetic converted to string
(first_int + second_int).to_s