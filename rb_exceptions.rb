# Exceptions in Ruby.

print "Enter a number: "
num_1 = gets.to_i

print "Enter a second number: "
num_2 = gets.to_i

# Start the group of code you expect an error in with "begin".
begin
    # Then, put the code you want to run.
    answer = num_1 / num_2

# Type rescue to start the error catching and give the message you want
# the user to receive or do whatever you need to to fix the issue.
rescue
    puts "You cannot divide by zero."
    exit
end

puts "#{num_1} / #{num_2} = #{answer}"



# We can also throw our own exceptions using raise

age = 12

def check_age(age)
    raise ArgumentError, "Enter positive number" unless (age > 0)
end

begin
    check_age(-1)

rescue ArgumentError
    puts "That is an invalid age."
end